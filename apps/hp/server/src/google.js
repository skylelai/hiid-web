const { google } = require('googleapis')

const { SPREADSHEET_ID } = process.env

let auth = null
let sheets = null

const _init = async () => {
  if (auth) {
    return
  }
  auth = await google.auth.getClient({
    scopes: ['https://www.googleapis.com/auth/spreadsheets'],
  })
  sheets = google.sheets({ version: 'v4', auth })
}

const get = async (ranges, majorDimension = 'ROWS') => {
  await _init()
  if (!Array.isArray(ranges)) {
    ranges = [ranges]
  }
  try {
    const response = await sheets.spreadsheets.values.batchGet({
      spreadsheetId: SPREADSHEET_ID,
      ranges,
      majorDimension,
    })
    const data = response.data.valueRanges.map((vr) => vr.values)
    return {
      error: null,
      data: data.length === 1 ? data[0] : data,
    }
  } catch (error) {
    return { error, data: null }
  }
}

const update = async (data) => {
  await _init()
  if (!Array.isArray(data)) {
    data = [data]
  }
  try {
    const response = await sheets.spreadsheets.values.batchUpdate({
      spreadsheetId: SPREADSHEET_ID,
      resource: {
        valueInputOption: 'USER_ENTERED',
        data,
      },
    })
    return { error: null, data: response.data.responses }
  } catch (error) {
    return { error, data: null }
  }
}

module.exports = { get, update }
