const { get, update } = require('./google')

const { isInt, isDecimal } = require('validator')
const T = {
  ARRAY_OPTIONS: /^array of options\((.*)\)$/,
  ARRAY_RANGE: /^array of range\((\d+),(\d+)\)$/,
  BOOLEAN: /^boolean$/,
  ID: /^id$/,
  FLOAT: /^float$/,
  INT: /^int$/,
  OPTIONS: /^options\((.*)\)$/,
  POSITIVE_FLOAT: /^positive float$/,
  POSITIVE_INT: /^positive int$/,
  RANGE: /^range\((\d+),(\d+)\)$/,
  STRING: /^string$/,
  UNIQUE_STRING: /^unique string$/,
}

const ssCol = (n) => {
  let s = '',
    t = null
  while (n > 0) {
    t = (n - 1) % 26
    s = String.fromCharCode(65 + t) + s
    n = ((n - t) / 26) | 0
  }
  return s || null
}

const getLoc = (majorDimension = 'ROWS') =>
  majorDimension === 'ROWS'
    ? (i, j) =>
        i === null
          ? `Column ${ssCol(j + 1)}`
          : j === null
          ? `Row ${i + 1}`
          : `${ssCol(j + 1)}${i + 1}`
    : (j, i) =>
        i === null
          ? `Column ${ssCol(j + 1)}`
          : j === null
          ? `Row ${i + 1}`
          : `${ssCol(j + 1)}${i + 1}`

const verifyTable = (dims, majorDimension = 'ROWS') => {
  const loc = getLoc(majorDimension)
  try {
    if (!dims) {
      throw 'no data'
    }
    const fieldNames = []
    const data = []
    for (let i = 0; i < dims.length; i++) {
      const dlen = dims[i].length
      if (dlen === 0) {
        throw `error at ${loc(i, null)}: type and field name is required`
      }
      const type = dims[i][0]
      if (i === 0 && !type.match(T.ID)) {
        throw `error at ${loc(0, 0)}: must be type "id"`
      }
      if (i > 0) {
        if (type.match(T.ID)) {
          throw `error at ${loc(
            i,
            0
          )}: one table can only have one field with "id" type`
        }
        if (
          ![
            T.ARRAY_OPTIONS,
            T.ARRAY_RANGE,
            T.BOOLEAN,
            T.FLOAT,
            T.INT,
            T.OPTIONS,
            T.POSITIVE_FLOAT,
            T.POSITIVE_INT,
            T.RANGE,
            T.STRING,
            T.UNIQUE_STRING,
          ].some((t) => type.match(t))
        ) {
          throw `error at ${loc(i, 0)}: unrecoginzed type "${type}"`
        }
      }
      if (dlen === 1) {
        throw `error at ${loc(i, 1)}: field name is required`
      }
      const field = dims[i][1]
      if (fieldNames.includes(field)) {
        throw `error at ${loc(i, 1)}: field name must be unique`
      } else {
        fieldNames.push(field)
      }
      if (dlen > dims[0].length) {
        throw `error at ${loc(i, null)}: all data need an id`
      }
      if (type.match(T.ID)) {
        for (j = 2; j < dlen; j++) {
          const val = dims[i][j]
          if (!isInt(val)) {
            throw `error at ${loc(i, j)}: id must be an int, not "${val}"`
          } else if (parseInt(val) !== j - 2) {
            throw `error at ${loc(i, j)}: should be ${j - 2}, not ${val}`
          }
          data.push({ [field]: j - 2, row: j + 1 })
        }
      } else if (type.match(T.ARRAY_OPTIONS)) {
        let [_, options] = type.match(T.ARRAY_OPTIONS)
        options = options.split(',')
        for (j = 2; j < dlen; j++) {
          let val = dims[i][j]
          if (val.trim() === '') {
            val = []
          } else {
            val = val.split(',')
            if (!val.every((v) => options.includes(v))) {
              throw `error at ${loc(i, j)}: every value must in [${options}]`
            }
          }
          data[j - 2][field] = val
        }
        for (j = dlen; j < data.length + 2; j++) {
          data[j - 2][field] = []
        }
      } else if (type.match(T.ARRAY_RANGE)) {
        let [_, l, r] = type.match(T.ARRAY_RANGE)
        l = parseInt(l)
        r = parseInt(r)
        if (l > r) {
          throw `error at ${loc(i, 0)}: range's 1st num must <= 2nd num`
        }
        for (j = 2; j < dlen; j++) {
          let val = dims[i][j]
          if (val.trim() === '') {
            val = []
          } else {
            val = val.split(',')
            if (!val.every((v) => isInt(v))) {
              throw `error at ${loc(i, j)}: every value must be an int`
            }
            val = val.map((v) => parseInt(v))
            if (!val.every((v) => v >= l && v <= r)) {
              throw `error at ${loc(i, j)}: every value must be in [${l}, ${r}]`
            }
          }
          data[j - 2][field] = val
        }
        for (j = dlen; j < data.length + 2; j++) {
          data[j - 2][field] = []
        }
      } else if (type.match(T.BOOLEAN)) {
        for (j = 2; j < dlen; j++) {
          let val = dims[i][j]
          if (val.trim() === '') {
            val = false
          } else if (['FALSE', 'TRUE'].includes(val)) {
            val = val === 'TRUE'
          } else {
            throw `error at ${loc(i, j)}: must be TRUE or FALSE`
          }
          data[j - 2][field] = val
        }
        for (j = dlen; j < data.length + 2; j++) {
          data[j - 2][field] = false
        }
      } else if (type.match(T.FLOAT)) {
        for (j = 2; j < dlen; j++) {
          let val = dims[i][j]
          if (val.trim() === '') {
            val = null
          } else if (isDecimal(val)) {
            val = parseFloat(val)
          } else {
            throw `error at ${loc(i, j)}: must be an int or float`
          }
          data[j - 2][field] = val
        }
        for (j = dlen; j < data.length + 2; j++) {
          data[j - 2][field] = null
        }
      } else if (type.match(T.INT)) {
        for (j = 2; j < dlen; j++) {
          let val = dims[i][j]
          if (val.trim() === '') {
            val = null
          } else if (isInt(val)) {
            val = parseInt(val)
          } else {
            throw `error at ${loc(i, j)}: must be an int`
          }
          data[j - 2][field] = val
        }
        for (j = dlen; j < data.length + 2; j++) {
          data[j - 2][field] = null
        }
      } else if (type.match(T.OPTIONS)) {
        let [_, options] = type.match(T.OPTIONS)
        options = options.split(',')
        for (j = 2; j < dlen; j++) {
          let val = dims[i][j]
          if (val.trim() === '') {
            val = ''
          } else if (!options.includes(val)) {
            throw `error at ${loc(i, j)}: value must in [${options}]`
          }
          data[j - 2][field] = val
        }
        for (j = dlen; j < data.length + 2; j++) {
          data[j - 2][field] = ''
        }
      } else if (type.match(T.POSITIVE_FLOAT)) {
        for (j = 2; j < dlen; j++) {
          let val = dims[i][j]
          if (val.trim() === '') {
            val = null
          } else if (isDecimal(val)) {
            val = parseFloat(val)
            if (val <= 0) {
              throw `error at ${loc(i, j)}: must be an int or float > 0`
            }
          } else {
            throw `error at ${loc(i, j)}: must be an int or float`
          }
          data[j - 2][field] = val
        }
        for (j = dlen; j < data.length + 2; j++) {
          data[j - 2][field] = null
        }
      } else if (type.match(T.POSITIVE_INT)) {
        for (j = 2; j < dlen; j++) {
          let val = dims[i][j]
          if (val.trim() === '') {
            val = null
          } else if (isInt(val)) {
            val = parseInt(val)
            if (val <= 0) {
              throw `error at ${loc(i, j)}: must be an int > 0`
            }
          } else {
            throw `error at ${loc(i, j)}: must be an int`
          }
          data[j - 2][field] = val
        }
        for (j = dlen; j < data.length + 2; j++) {
          data[j - 2][field] = null
        }
      } else if (type.match(T.RANGE)) {
        let [_, l, r] = type.match(T.RANGE)
        l = parseInt(l)
        r = parseInt(r)
        if (l > r) {
          throw `error at ${loc(i, 0)}: range's 1st num must <= 2nd num`
        }
        for (j = 2; j < dlen; j++) {
          let val = dims[i][j]
          if (val.trim() === '') {
            val = null
          } else {
            if (!isInt(val)) {
              throw `error at ${loc(i, j)}: value must be an int`
            }
            val = parseInt(val)
            if (!(val >= l && val <= r)) {
              throw `error at ${loc(i, j)}: value must be in [${l}, ${r}]`
            }
          }
          data[j - 2][field] = val
        }
        for (j = dlen; j < data.length + 2; j++) {
          data[j - 2][field] = null
        }
      } else if (type.match(T.STRING)) {
        for (j = 2; j < dlen; j++) {
          let val = dims[i][j]
          if (val.trim() === '') {
            val = ''
          }
          data[j - 2][field] = val
        }
        for (j = dlen; j < data.length + 2; j++) {
          data[j - 2][field] = ''
        }
      } else if (type.match(T.UNIQUE_STRING)) {
        const pool = []
        for (j = 2; j < dlen; j++) {
          let val = dims[i][j]
          if (val.trim() === '') {
            val = ''
          } else if (pool.includes(val)) {
            throw `error at ${loc(i, j)}: value must be unique`
          } else {
            pool.push(val)
          }
          data[j - 2][field] = val
        }
        for (j = dlen; j < data.length + 2; j++) {
          data[j - 2][field] = ''
        }
      }
    }
    return { data }
  } catch (error) {
    return { error }
  }
}

const getUserByCode = async (code) => {
  const [
    { error: errorTables, data: dataTables },
    { error: errorSetting, data: dataSetting },
  ] = await Promise.all([
    get(['houses!A1:S', 'problems!A1:C', 'users!A1:H'], 'COLUMNS'),
    get(['settings!A1:C']),
  ])
  if (errorTables || errorSetting) {
    console.log(errorTables || errorSetting)
    return errorTables || errorSetting
  }
  try {
    const { error: errorHouses, data: houses } = verifyTable(
      dataTables[0],
      'COLUMNS'
    )
    if (errorHouses) {
      console.log(`table houses ${errorHouses}`)
      throw `table houses ${errorHouses}`
    }
    let { error: errorProblems, data: problems } = verifyTable(
      dataTables[1],
      'COLUMNS'
    )
    if (errorProblems) {
      console.log(`table problems ${errorProblems}`)
      throw `table problems ${errorProblems}`
    }
    let { error: errorUsers, data: users } = verifyTable(
      dataTables[2],
      'COLUMNS'
    )
    if (errorUsers) {
      console.log(`table users ${errorUsers}`)
      throw `table users ${errorUsers}`
    }
    const { error: errorSettings, data: settings } = verifyTable(dataSetting)
    if (errorSettings) {
      console.log(`table settings ${errorSettings}`)
      throw `table settings ${errorSettings}`
    }
    // console.log(houses, problems, users, settings[0]);

    const user = users.find((u) => u.code === code)
    if (!user) {
      throw 'code not found'
    }
    user.problems = user.problemIds.map((id) => ({
      h1: houses[problems[id].h1],
      h2: houses[problems[id].h2],
    }))
    return {
      data: {
        user,
        settings: settings.map((setting) => ({
          ...setting,
          practices: setting.practiceProblemIds.map((id) => ({
            h1: houses[problems[id].h1],
            h2: houses[problems[id].h2],
          })),
        }))[0],
      },
    }
  } catch (error) {
    return { error: { message: error } }
  }
}

const updateUserByCode = async ({
  code,
  higherAnswers,
  confidenceLevels,
  infoFeeDone,
  timeRecording,
}) => {
  try {
    if (!code) {
      throw 'code is required'
    }
    const { error, data: dataCode } = await get([`users!B3:B`], 'COLUMNS')
    if (error) {
      return { error }
    }
    const row = dataCode[0].indexOf(code) + 3
    if (row === 2) {
      throw 'code not found'
    }
    const updates = []
    if (higherAnswers !== undefined) {
      if (
        !Array.isArray(higherAnswers) ||
        !higherAnswers.every((a) => ['h1', 'h2'].includes(a))
      ) {
        throw 'higherAnswers must be array of options(h1 or h2)'
      }
      updates.push({
        range: `users!E${row}`,
        values: [[higherAnswers.join(',')]],
      })
    }
    if (confidenceLevels !== undefined) {
      if (
        !Array.isArray(confidenceLevels) ||
        !confidenceLevels.every(
          (a) => isInt(`${a}`) && parseInt(a) >= 1 && parseInt(a) <= 7
        )
      ) {
        throw 'confidenceLevels must be array of range(1 ~ 7)'
      }
      updates.push({
        range: `users!F${row}`,
        values: [[confidenceLevels.join(',')]],
      })
    }
    if (infoFeeDone !== undefined) {
      if (![true, false].includes(infoFeeDone)) {
        throw 'infoFeeDone must be boolean'
      }
      updates.push({
        range: `users!G${row}`,
        values: [[infoFeeDone ? 'TRUE' : 'FALSE']],
      })
    }
    if (timeRecording !== undefined) {
      const parseTR = timeRecording.split(',')
      if (
        parseTR.length !== 5 ||
        !parseTR.every((ts) =>
          ts.split(';').every((t) => isInt(t) && parseInt(t) >= 0)
        )
      ) {
        throw 'timeRecording must be array of (array of int >= 0 join by ";") join by "," with length = 5'
      }
      updates.push({
        range: `users!H${row}`,
        values: [[timeRecording]],
      })
    }
    console.log(updates)
    const { error: error2, data: dataUpdate } = await update(updates)
    if (error2) {
      return { error: error2 }
    }
    return { success: true }
  } catch (error) {
    return { error: { message: error } }
  }
}

module.exports = {
  getUserByCode,
  updateUserByCode,
}
