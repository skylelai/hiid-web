const { getUserByCode, updateUserByCode } = require('./spreadsheet')

module.exports = [
  {
    route: '/user/:userCode',
    method: 'get',
    async handler(req, res) {
      const { userCode } = req.params
      if (!userCode) {
        return res
          .status(400)
          .send({ error: { message: 'userCode is required' } })
      }
      const { error, data } = await getUserByCode(userCode)
      if (error) {
        console.log(error)
        return res.status(400).send({ error })
      } else {
        return res.send({ data })
      }
    },
  },
  {
    route: '/user',
    method: 'post',
    async handler(req, res) {
      const result = await updateUserByCode(req.body)
      if (result.error) {
        console.log(result.error)
        return res.status(400).send(result)
      } else {
        return res.send(result)
      }
    },
  },
]
