const path = require('path')
require('dotenv').config({ path: path.join(__dirname, './.env.local') })

const express = require('express')
const server = express()

server.use(require('compression')())
server.use(require('cors')())
server.use(express.json())
server.use(express.urlencoded({ extended: true }))

const api = require('./src/api')
api.forEach(({ route, handler, method = 'get' }) =>
  server[method](`/api${route}`, handler)
)

const dist = path.join(__dirname, '../dist')
server.use(express.static(dist))
server.get('*', (req, res) => {
  res.sendFile(path.join(dist, 'index.html'))
})

const port = process.env.PORT || 8888
server.listen(port, () => {
  console.log(`Server started: http://localhost:${port}`)
})
