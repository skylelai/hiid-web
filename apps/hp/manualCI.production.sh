rm -rf ./dist
yarn build:production
mkdir -p ./hp
cp -r ./dist ./server ./package.json ./hp
mv ./hp/server/.env.test.local ./hp/server/.env.local
yarn --cwd ./hp rm:devdep && yarn --cwd ./hp install
rsync -raz --stats ./hp root@172.104.93.121:/root/app/apps --delete
ssh root@172.104.93.121 docker exec app_hp_1 /run_pm2.sh
rm -rf ./hp