const fs = require('fs')

const getRandomInt = (max) => Math.floor(Math.random() * max)
const getRandomString = (len) => {
  const AZ = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
  let out = ''
  for (let i = 0; i < len; i++) {
    out += AZ[getRandomInt(26)]
  }
  return out
}
const shuffle = (arr) => {
  const len = arr.length
  for (let i = 0; i < len; i++) {
    const a = getRandomInt(len)
    const b = getRandomInt(len)
    const t = arr[a]
    arr[a] = arr[b]
    arr[b] = t
  }
}
const IND = 2500000
const getLevel = (d) =>
  d < IND
    ? 1
    : d < IND * 2
    ? 2
    : d < IND * 3
    ? 3
    : d < IND * 4
    ? 4
    : d < IND * 5.5
    ? 5
    : d < IND * 7
    ? 6
    : d < IND * 9
    ? 7
    : d < IND * 12
    ? 8
    : d < IND * 20
    ? 9
    : 10
const zh2arNum = {
  五: 5,
  十三: 13,
  全: '全',
  四: 4,
  一: 1,
  九: 9,
  十一: 11,
  十二: 12,
  十六: 16,
  三: 3,
  七: 7,
  二: 2,
  十八: 18,
  八: 8,
  十九: 19,
  十: 10,
  六: 6,
  十七: 17,
  十四: 14,
  十五: 15,
  二十五: 25,
  二十一: 21,
  二十四: 24,
  二十二: 22,
  二十七: 27,
  二十: 20,
  二十三: 23,
  二十六: 26,
  二十九: 29,
  地下一: '地下 1',
  地下二: '地下 2',
  三十: 30,
  三十三: 33,
  二十八: 28,
  四十二: 42,
  地下三: '地下 3',
  三十一: 31,
  三十二: 32,
  三十六: 36,
  三十七: 37,
  四十一: 41,
  三十五: 35,
  三十四: 34,
  四十: 40,
  四十三: 43,
  四十六: 46,
  三十八: 38,
}

const lines = fs.readFileSync('data_with_pred.csv', 'utf-8').split('\n')
const houses = []
for (let i = 1; i < lines.length; i++) {
  const line = lines[i].split(',')
  if (!zh2arNum[line[14]] || !zh2arNum[line[15]]) {
    console.log(
      `skip csvId ${i - 1}: unrecognized floor (${line[14]} or ${line[14]})`
    )
    continue
  }
  houses.push({
    csvId: i - 1,
    groundTruth: parseInt(line[9]),
    prediction: Math.round(parseFloat(line[25])),
    location: `${line[2]}${line[1]}`,
    material: line[3],
    type: line[23],
    floor: `${zh2arNum[line[14]]} / ${zh2arNum[line[15]]}`,
    landTrans: parseFloat(line[20]),
    buildingTrans: parseFloat(line[21]),
    mainBuilding: parseFloat(line[10]),
    subBuilding: parseFloat(line[11]),
    balcony: parseFloat(line[12]),
    common: parseFloat(line[22]),
    age: parseInt(line[16]),
    parking: parseFloat(line[19]),
    partition: line[7] === '有',
    structure: `${line[4]} / ${line[5]} / ${line[6]}`,
    org: line[8] === '有',
  })
}

const sectionHouses = houses.reduce((prev, curr) => {
  const key = `${curr.location}${curr.material}`
  if (prev[key]) {
    prev[key].push(curr)
  } else {
    prev[key] = [curr]
  }
  return prev
}, {})

const sectionDistribution = Object.keys(sectionHouses).reduce((prev, curr) => {
  // original number
  const on = ((sectionHouses[curr].length / houses.length) * 368) / 4
  const bd = Math.floor(on) // before dot
  const ad = on - bd // after dot
  const th = 0.375 // threshold
  prev[curr] = (bd + (ad >= th ? 1 : 0)) * 4
  return prev
}, {})
const sectionDistributionSum = Object.keys(sectionDistribution).reduce(
  (prev, curr) => prev + sectionDistribution[curr],
  0
)
// console.log(sectionDistributionSum) // test threshold until sum === 368
// console.log(sectionDistribution)
const levelDistribution = {
  1: 40,
  2: 36,
  3: 26,
  4: 20,
  5: 18,
  6: 12,
  7: 10,
  8: 8,
  9: 10, // 6 + 4(practice)
  10: 4,
}
const getHighestLevel = (max = 10) => {
  for (let i = max; i >= 1; i--) {
    if (levelDistribution[i] > 0) {
      return i
    }
  }
  return 0
}
const selectedCsvIds = []
const getRandom2 = (arr, isCorrect) => {
  let a, b, level
  let currentLevel = getHighestLevel()
  let tries = 0
  do {
    // match highest level first, try 10000 times
    if (tries > 10000) {
      tries = 0
      currentLevel = getHighestLevel(currentLevel - 1)
      if (currentLevel === 0) {
        console.log('----------error----------')
        break
      }
    }
    a = getRandomInt(arr.length)
    b = getRandomInt(arr.length)
    const aa = arr[a]
    const ab = arr[b]
    level = getLevel(Math.abs(aa.groundTruth - ab.groundTruth))
    if (
      a !== b &&
      !selectedCsvIds.includes(aa.csvId) &&
      !selectedCsvIds.includes(ab.csvId) &&
      levelDistribution[level] > 0
    ) {
      // skip not highest level
      if (level !== currentLevel) {
        tries++
        continue
      }
      if (isCorrect) {
        if (
          (aa.groundTruth > ab.groundTruth && aa.prediction > ab.prediction) ||
          (aa.groundTruth < ab.groundTruth && aa.prediction < ab.prediction)
        ) {
          break
        }
      } else {
        if (
          (aa.groundTruth > ab.groundTruth && aa.prediction < ab.prediction) ||
          (aa.groundTruth < ab.groundTruth && aa.prediction > ab.prediction)
        ) {
          break
        }
      }
    }
  } while (true)
  levelDistribution[level]--
  selectedCsvIds.push(arr[a].csvId)
  selectedCsvIds.push(arr[b].csvId)
  return [arr[a], arr[b], isCorrect, level]
}

const output = []
const sectionDistributionKeys = Object.keys(sectionDistribution)
shuffle(sectionDistributionKeys)
sectionDistributionKeys.forEach((section) => {
  const count = sectionDistribution[section]
  if (count) {
    console.log(`trying section: ${section}`)
    // console.log(levelDistribution)
    for (let i = 0; i < count / 4; i++) {
      output.push(getRandom2(sectionHouses[section], true))
      output.push(getRandom2(sectionHouses[section], false))
    }
  }
})
shuffle(output)

const getHouseLine = (id, h) => {
  const params = [
    'csvId',
    'groundTruth',
    'prediction',
    'location',
    'material',
    'type',
    'floor',
    'landTrans',
    'buildingTrans',
    'mainBuilding',
    'subBuilding',
    'balcony',
    'common',
    'age',
    'parking',
    'partition',
    'structure',
    'org',
  ]
  return [id, ...params.map((p) => h[p])]
}
const outHouses = []
const outProblems = []
output.forEach(([h1, h2, isCorrect, level], idx) => {
  outHouses.push(getHouseLine(idx * 2, h1).join(','))
  outHouses.push(getHouseLine(idx * 2 + 1, h2).join(','))
  outProblems.push([idx, idx * 2, idx * 2 + 1, isCorrect, level].join(','))
})

fs.writeFileSync('houses.csv', outHouses.join('\n'))
fs.writeFileSync('problems.csv', outProblems.join('\n'))

const outUsers = []
const includeCodes = []
for (let i = 0; i < 210; i++) {
  let code = getRandomString(4)
  while (includeCodes.includes(code)) {
    code = getRandomString(4)
  }
  includeCodes.push(code)

  const problemIds = []
  for (let j = i; j < i + 48; j++) {
    problemIds.push(j % 180)
  }
  problemIds.splice(28, 0, 180)
  problemIds.splice(45, 0, 181)
  outUsers.push([i, code, `"${problemIds.join(',')}"`].join(','))
}

fs.writeFileSync('users.csv', outUsers.join('\n'))
