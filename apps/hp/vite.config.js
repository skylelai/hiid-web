import path from 'path'

import vue from '@vitejs/plugin-vue'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { AntDesignVueResolver } from 'unplugin-vue-components/resolvers' // antdv
import Icons from 'unplugin-icons/vite'
import Pages from 'vite-plugin-pages'
import Layouts from 'vite-plugin-vue-layouts'

export default {
  css: { preprocessorOptions: { less: { javascriptEnabled: true } } }, // antdv
  plugins: [
    vue(),
    AutoImport({
      imports: ['vue', 'vue-router', '@vueuse/head', '@vueuse/core'],
    }),
    Components({
      dirs: ['src/components', 'src/pages/**/components'],
      resolvers: [AntDesignVueResolver({ importStyle: false })],
    }), // antdv
    Icons({}),
    Pages({ exclude: ['**/components/*.vue'] }),
    Layouts(),
  ],
  resolve: { alias: { '@': path.resolve(__dirname, './src') } },
}
