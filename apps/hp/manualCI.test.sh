rm -rf ./dist
yarn build
mkdir -p ./hp-test
cp -r ./dist ./server ./package.json ./hp-test
mv ./hp-test/server/.env.test.local ./hp-test/server/.env.local
yarn --cwd ./hp-test rm:devdep && yarn --cwd ./hp-test install
rsync -raz --stats ./hp-test root@172.104.66.117:/root/app/apps --delete
ssh root@172.104.66.117 docker exec app_hp-test_1 /run_pm2.sh
rm -rf ./hp-test