import { createFetch } from '@vueuse/core'

export const useMyFetch = createFetch({
  baseUrl: import.meta.env.VITE_API_BASE_URL,
})
